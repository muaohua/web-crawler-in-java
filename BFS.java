/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webCrawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author bruce
 */
public class BFS {
    private Queue<String> queue;
    private List<String> discoveredWebList;
    
    public BFS(){
        this.queue = new LinkedList<String>();
        this.discoveredWebList = new ArrayList<String>();
    }
    
    public void discoverWebsite(String root){
        this.queue.add(root);
        this.discoveredWebList.add(root);
        
        while(!this.queue.isEmpty()){
            String urlString = this.queue.remove();
            String rawHtml = readURL(urlString);
            
            String regexp = "http://(\\w+\\.)*(\\w+)";
            Pattern pattern = Pattern.compile(regexp);
            Matcher matcher = pattern.matcher(rawHtml);
            
            while(matcher.find()){
                String w = matcher.group();
                
                if(!this.discoveredWebList.contains(w)){
                    this.discoveredWebList.add(w);
                    this.queue.add(w);
                    System.out.println("Website found with URL: "+w);
                }
            }
        }
    }

    private String readURL(String urlString) {
        String rawHtml = "";
        try {
            
            URL url = new URL(urlString);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            
            String inputLine = "";
            
            while((inputLine = br.readLine())!=null){
                rawHtml += inputLine;
            }
            
            br.close();
        } catch (MalformedURLException ex) {
            Logger.getLogger(BFS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BFS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rawHtml;
    }
}
